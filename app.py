# We need 'datetime' module for getting the current time
# We need the 're' module to validate email address using regular expressions
# We import 'Flask' to create an app,
# 'request' to access the request data in each view
# 'flash' to send messages to be displayed in the template
# 'url_for' to get the URL for a given view function name
# 'redirect' to redirect to a given URL
# 'render_template' to render a template
# 'flask_sqlalchemy' provides a wrapper over SQLAlchemy. Install
# 'flask-sqlalchemy' package to use this.
import os
import time
from datetime import datetime
import re
import json
from json import JSONEncoder
from flask import Flask, request, flash, url_for, redirect, render_template, session, Response
from flask_sqlalchemy import SQLAlchemy
from flask_oauth import OAuth
import ConfigParser

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Creates a Flask app and reads the settings from a 
# configuration file. We then connect to the database specified
# in the settings file

app = Flask(__name__)
app.config.from_pyfile(os.path.join(BASE_DIR, 'app.cfg'))
db = SQLAlchemy(app)

config = ConfigParser.ConfigParser()
try:
  config.read(os.path.join(BASE_DIR, 'gauth.cfg'))
  GOOGLE_CLIENT_ID = config.get("Google Auth","GOOGLE_CLIENT_ID")
  GOOGLE_CLIENT_SECRET = config.get("Google Auth","GOOGLE_CLIENT_SECRET")
  REDIRECT_URI = config.get("Google Auth","REDIRECT_URI")
except:
  print "no Google authentication configuration file"

oauth = OAuth()

google = oauth.remote_app('google',
                          base_url='https://www.google.com/accounts/',
                          authorize_url='https://accounts.google.com/o/oauth2/auth',
                          request_token_url=None,
                          request_token_params={'scope': 'https://www.googleapis.com/auth/userinfo.email',
                                                'response_type': 'code'},
                          access_token_url='https://accounts.google.com/o/oauth2/token',
                          access_token_method='POST',
                          access_token_params={'grant_type': 'authorization_code'},
                          consumer_key=GOOGLE_CLIENT_ID,
                          consumer_secret=GOOGLE_CLIENT_SECRET)
# We are defining a 'Users' model to store the Users the user
# enters via the form.
class Users(db.Model):
  # Setting the table name and
  # creating columns for various fields
  __tablename__ = 'users'
  id = db.Column('user_id', db.Integer, primary_key=True)
  email = db.Column(db.String(100))
  token = db.Column(db.String(100))
  login_time = db.Column(db.DateTime)
  number = db.Column(db.Integer)
  
  def __init__(self, email, token):
      # Initializes the fields with entered data
      # and sets the published date to the current time
      self.email = email
      self.token = token
      self.login_time = datetime.now()
      self.number = -1

def is_number_valid(number):
  """Validate number using regular expression."""
  if not re.match("^[0-9]*$", number):
      return False
  return True

# The default route for the app. 
# Displays the list of already entered users
# We are getting all the users ordered in 
# descending order of login_time and passing to the
# template via 'users' variable
# return value explained:
# 0 -> redirect to index
# 1 -> redirect to login
# -1 -> redirect to error
@app.route('/', methods=['GET', 'POST'])
def index():
  ret = 0
  user_1 = None
  user_2 = None
  # create db if not existed
  try:
    Users.query.all()
  except:
    db.create_all()
  cur_user_count = int(Users.query.count())
  # get token to request userinfo
  access_token = session.get('access_token')
  if access_token is None:
    if cur_user_count < 2:
      ret = 1
    else:
      flash('Error: The number of attendee is maximimized!', 'error')
      ret = -1
  if ret == 0:
    access_token = access_token[0]

    if request.method == 'POST':
      if not request.form['number']:
        flash('Error: Please enter the number', 'error')
        ret = -1
      elif not is_number_valid(request.form['number']):
        flash('Error: Please enter a valid number', 'error')
        ret = -1
      else:
        Users.query.filter_by(token=access_token).update(dict(number=int(request.form['number'])))
        db.session.commit()
    else:
      from urllib2 import Request, urlopen, URLError

      headers = {'Authorization': 'OAuth '+access_token}
      req = Request('https://www.googleapis.com/oauth2/v1/userinfo', None, headers)
      try:
        res = urlopen(req)
        data = json.load(res)
        email_match_list = Users.query.filter_by(email=data["email"]).all()
        if email_match_list == []:
          user = Users(data["email"],access_token)
          db.session.add(user)
          db.session.commit()            
        else:
          user_index = Users.query.filter_by(email=data["email"]).update(dict(token=access_token))
          db.session.commit()
      except URLError, e:
        if e.code == 401:
          # Unauthorized - bad token
          session.pop('access_token', None)
          ret = 1
    if ret == 0:
      user_1 = Users.query.get(1)
      cur_user_count = int(Users.query.count())
      if cur_user_count > 1:
        user_2 = Users.query.get(2)
      else:
        user_2 = None

  if ret == 0:
    return render_template('index.html', user1=user_1, user2=user_2,token=access_token)
  elif ret == 1:
    return redirect(url_for('login'))
  else:
    return render_template('error.html')

@app.route('/login')
def login():
    callback=url_for('authorized', _external=True)
    return google.authorize(callback=callback)

@app.route(REDIRECT_URI)
@google.authorized_handler
def authorized(resp):
    access_token = resp['access_token']
    session['access_token'] = access_token, ''
    return redirect(url_for('index'))

@app.route('/update')
def update():
    def check():
      print "DEBUG: GET /update"
      result = ""
      user1_ready = "No"
      user2_email = "N/A"
      user2_login_time = "N/A"
      user2_ready = "No"
      cur_user_count = int(Users.query.count())
      user_1 = Users.query.get(1)
      if user_1.number != -1:
          user1_ready = "Yes"
      if cur_user_count > 1:
        user_2 = Users.query.get(2)
        user2_email = user_2.email
        user2_login_time = user_2.login_time.strftime("%Y-%m-%d %H:%M:%S")
        if user_2.number != -1:
          user2_ready = "Yes"
        if user_1.number != -1 and user_2.number != -1:
          result = "The winner is "
          winner = None
          
          if user_1.number > user_2.number:
            winner = user_1.email
          elif user_2.number > user_1.number:
            winner = user_2.email
          else:
            winner = "N/A"
          result = result + winner
      jsonString = JSONEncoder().encode({
      "user1_ready": user1_ready,
      "user2_email": user2_email, 
      "user2_login_time": user2_login_time,
      "user2_ready":  user2_ready,
      "result": result
      })
      yield "data:" + str(jsonString) + "\n\n"
    return Response(check(), mimetype='text/event-stream')  
# This is the code that gets executed when the current python file is
# executed. 
if __name__ == '__main__':
	app.run( 
        host="0.0.0.0",
        port=int("5000"),
        debug=True   
  )
